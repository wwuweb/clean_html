<?php

function htmlpurifier_config_clean_html($config) {
	// Enable anchor IDs
	$config->set('Attr.EnableID', true);
}
